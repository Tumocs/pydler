#content file imported by main.py

#LOCATION
class Location:
    
    def __init__ (self, name):
        self.name = name
        self.stuffs = []
        self.nesw = []	
        self.open = bool
        self.descript = ""
        self.unlockable = bool
        self.blockdescript = ""

    def add_stuff (self, *stuff):
        self.stuffs.append(stuff)

#SET GAME LOCATIONS
room = Location('Minze\'s room')
diner = Location('Diner')
wall = Location('Wall')
hallway = Location('Hallway')
storage = Location('Storage room')
wc = Location('WC')
freedom = Location('FREEDOOOM')

#ITEM CLASS
class Item:
    def __init__ (self, name):
        self.name = name
        self.descript = ""
        self.position = ""
        self.pickable = bool

        self.mutable = bool
        self.CombineWith = ""
        self.CombinesTo = ""
        self.useable = bool
        self.TargetOf = ""
        self.usefunction = ""
        self.UnlockRoom = ""
        self.usedescript = ""
        self.usedescript2 = ""
        self.blocked = bool
        self.blocker = ""
        self.BlockedDescript = ""
        self.MutatesTo = ""
        self.MoveTarget = ""
        self.MovesTo = []
        self.dead = bool

usefunctions = ["unlock", "mutate", "move"]

#SET GLOBAL ITEMS

button = Item('Button')
finger = Item('Finger')
sheet = Item('Sheet')
handle = Item('Handle')
shivhandle = Item('Shivhandle')
shiv = Item('Shiv')
knife = Item('Knife')
glass = Item('Glass')
key = Item('Key')
lock = Item('Lock')
nurse = Item('Nurse')
bouncer = Item('Bouncer')
wheelchair = Item('Wheelchair')
poop = Item('Poop')
microwave = Item('Microwave')
null = Item('nullblocker')

finger.useable = True
finger.mutable = False
finger.descript = "It's your own dirty finger. It smells a bit poopish."

#Room options
room.stuffs = [button, sheet]
room.nesw = [wc, wall, wall, hallway]
room.descript = "A bleak hospital room where you have spen't too long time. \nSmell is horrid and the flickering lights make you nauseous"
room.open = True

#Room object options
button.pickable = False
button.TargetOf = finger
button.usefunction = "move"
button.position = "next to hard hospital bed on rotting night table"
button.descript = "Red alarm button. Look tempting to press with your finger"
button.usedescript = "You sound an alarm, nurse rushes in to see what's wrong and gets disgusted by all your shit."
button.usedescript2 = "The alarm is sounded but no one comes to check it out"
button.MoveTarget = nurse
button.blocker = null

sheet.pickable = True
sheet.mutable = True
sheet.CombineWith = knife
sheet.CombinesTo = shivhandle
sheet.descript = "White and hard hospital sheet. \nVery uncomfortable to sleep with. Could be used to bind something."
sheet.position = "on top of horrible hospital bed"

key.position = "on the puddle of nurse\'s blood."
key.pickable = True
key.useable = True
key.mutable = False
key.descript = "Bloody keychain. You bet you could fit one of these to the storage room lock."

#WC options
wc.stuffs = [handle, poop]
wc.nesw = [wall, wall, room, wall]
wc.descript = "Hideous bathroom that has never been cleaned."
wc.open = True

#WC item options
poop.pickable = False
poop.position = "on the wall"
poop.descript = "Looks like the poop you threw there yesterday. You can see some corn in it. \nStrange. You don't remember eating corn."

handle.pickable = True
handle.mutable = False
handle.useable = True
handle.position = "next to sink"
handle.descript = "A metal handshower handle. It looks like you could you could detach and take it."

#Hallway options
hallway.open = True
hallway.stuffs = [lock, bouncer]
hallway.nesw = [freedom, room, storage, diner]
hallway.descript = "Short hallway with sweet freedom waiting for you in the north exit"

#Hallway item options
bouncer.pickable = False
bouncer.TargetOf = wheelchair
bouncer.UnlockRoom = freedom
bouncer.usefunction = "unlock"
bouncer.position = "blocking your freedom"
bouncer.descript = "Horrible manbeast that doesn't want to let you pass, at least not without wheelchair"
bouncer.usedescript = "You beat the idiot manbeast to pulp with your wheelchair. The road to freedom is finally open."
bouncer.blocker = null

lock.pickable = False
lock.TargetOf = key
lock.usefunction = "unlock"
lock.UnlockRoom = storage
lock.position = "mockingly dangling on storage door."
lock.description = "Strong lock you can't possibly hope to break with your feeble arms. \n Better to look for a key."
lock.usedescript = "The storage lock pops open and drops to floor with loud clink. Bouncer doesn't seem to care."
lock.blocker = null

#Diner options
diner.open = True
diner.stuffs = [microwave, nurse, knife]
diner.nesw = [wall, hallway, wall, wall]
diner.descript = "large diner with long white tables and chairs so hard that you'd rather stand."

#Diner item options
microwave.pickable = False
microwave.TargetOf = handle
microwave.usefunction = "mutate"
microwave.usedescript = "You put metal handle to microwave and it hisses and fizzes and sparkles until it finally explodes. \nYou find pieces of glass around you"
microwave.descript = "It's an ancient microwave oven with a door made of glass."
microwave.blocked = True
microwave.blocker = nurse
microwave.BlockedDescript = "You can't do that while the nurse is watching"
microwave.MutatesTo = glass
microwave.dead = False

glass.pickable = True
glass.mutable = True
glass.CombineWith = shivhandle
glass.CombinesTo = shiv
glass.position = "is spread all around you."
glass.descript = "You see a nice piece of glass, you could possibly stab someone with it if you only had a handle to attach it to."

nurse.pickable = False
nurse.usefunction = "mutate"
nurse.usedescript = "You stab nurse furiously sending blood splatters everywhere. In your murderous rage you hear a faint clink. \nKeys have dropped on the floor"
nurse.TargetOf = shiv
nurse.descript = "The most annoying nurse you have ever seen. Xer only feeds you healthy food and takes care of you."
nurse.position = "loudly slurping coffee in the corner."
nurse.MutatesTo = key
nurse.blocker = null
nurse.MovesTo = [diner, room]
nurse.dead = False

knife.pickable = True
knife.mutable = True
knife.CombineWith = sheet
knife.CombinesTo = shivhandle
knife.position = "on the long table."
knife.descript = "A plastic knife you couldn't hope to cut anything with. \n You might be able to use it as a handle for something sharper."

shivhandle.mutable = True
shivhandle.CombineWith = glass
shivhandle.CombinesTo = shiv
shivhandle.descript = "A makeshift handle you could attach something sharp to."

shiv.useable = True
shiv.descript = "A sharp shiv you could possibly kill someone with if you were a nasty psychopath."

#Storage options
storage.open = False
storage.nesw = [hallway, wall, wall, wall]
storage.description = "Gray and dusty storage room filled with cobweb and rusty barely functional wheelchairs"
storage.stuffs = [wheelchair]
storage.blockdescript = "The door is locked, you can't force your way through"

#Storage item options
wheelchair.pickable = True
wheelchair.mutable = False
wheelchair.useable = True
wheelchair.position = "on the floor."
wheelchair.descript = "Rusty old wheelchair, you doubt you could move with it even if you wanted to."

#Freedom options
freedom.open = False
freedom.blockdescript = "The bouncer steps in to your way and pushes you back. \nHe spits on your weak body."
freedom.stuffs = [" "]
freedom.nesw = [" "]

#wall options
wall.open = False
wall.blockdescript = "You bump your head into a wall.\nIt brings you no satisfaction."
wall.stuffs = [" "]
wall.nesw = [" "]

#UNLOCKING FUNCTION
def unlock(item):
    item.UnlockRoom.open = True
    print(item.usedescript)

#MUTATE FUNCTION
def mutate(item, location):
    item.dead = True
    print(item.usedescript)
    location.stuffs.append(item.MutatesTo)	

#MOVE FUNCTION
def move(item, location):
    if item.MoveTarget.dead == True:
        print(item.usedescript2)
    elif item.MoveTarget.dead == False:
        print(item.usedescript)
        location.stuffs.append(item.MoveTarget)
        item.MoveTarget.MovesTo[0].stuffs.remove(item.MoveTarget)

#SET GAME START parameters
start_location = room
initial_inventory = [finger, ]
wincondition = freedom
wintext = "Minze has escaped the horrible hospital and is free again!"
