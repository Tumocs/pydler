#! /usr/bin/python3
#
#    PYDLER a toddler tier text adventure engine in python.
#    Copyright (C) 2018
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import content

commands = ["look", "take", "inventory", "quit", "move","use", "combine", "help", "examine"]

current_location = content.start_location
        
inventory = content.initial_inventory

#HELP FUNCTION
def help(target):
    if target == "help":
        print("Try again you cheeky git")
    if target == "look":
        print("Use look to look aroung. USAGE: look")
    if target == "take":
        print("Use take to pick up objects. USAGE: take <object>")
    if target == "inventory":
        print("Use inventory to see what you are carrying with you. USAGE: inventory")
    if target == "quit":
        print("Use quit to quit the game. USAGE: quit")
    if target == "move":
        print("Use move to move around. USAGE: move <direction> . Direction can be (N)orth (E)ast (S)outh or (W)est.")
    if target == "combine":
        print("Use combine to combine different objects in your inventory. USAGE: combine <object> with <object>")
    if target == "examine":
        print("Ese examine to examine objects. USAGE: examine <object>")
    if target == "use":
        print("Use inventory items with world objects. USAGE: use <inventory item> with <object>")

#LOOK FUNCTION
def look(location):
    i = -1
    print("You are in {}.".format(location.name))
    print("You see")
    for item in location.stuffs:	
        i += 1
        print(location.stuffs[i].name + " " + location.stuffs[i].position)
    print("In north is {} \n in east is {} \n in south is {} \n in west is {}. ".format(location.nesw[0].name, location.nesw[1].name, location.nesw[2].name, location.nesw[3].name))

#TAKE FUNCTION
def take(stuffs, target):
    b = -1
    for item in stuffs:
        b += 1
        if target.lower() == stuffs[b].name.lower() and stuffs[b].pickable == True:
            print("You pick up {}".format(stuffs[b].name))
            inventory.append(stuffs.pop(b))
            break
        if target.lower() == stuffs[b].name.lower() and stuffs[b].pickable == False:
            print("You can't pick that up")
            break
        elif stuffs[b].name == stuffs[-1].name and stuffs[b].name.lower() != target.lower():
            print("You can't pick that up")
            break

#COMBINE ITEMS TOGETHER
def combine(inv, item_a, item_b):
    c = -1
    for item in inv:
        c += 1
        if inv[c].name.lower() == item_a.lower() and inv[c].mutable == False:
            print("{} doesn't look like it can be combined with anything".format(inv[c].name))
            break
        if inv[c].name.lower() == item_a.lower() and item_b.lower() == inv[c].CombineWith.name.lower():
            d = -1
            for item in inv:
                d += 1
                if inv[d].name.lower() == item_b.lower():
                    removable1 = inv[c]
                    removable2 = inv[d]
                    print("You combine {} with {} and create {}".format(inv[c].name, inv[d].name, inv[c].CombinesTo.name))
                    inv.append(inventory[c].CombinesTo)
                    inv.remove(removable1)
                    inv.remove(removable2)
                    break
                elif inv[d].name == inv[-1].name and item_b.lower() != inv[d].name.lower():
                    print("Looks like you don't have {} yet".format(item_b))
                    break
        elif inv[c].name.lower() == item_a.lower() and inv[c].CombineWith.name.lower() != item_b.lower():
            print("can't put those together")
            break

def use(invitem, target, inventory, location):
    i = -1
    b = -1
    present = []
    for item in location.stuffs:
        b += 1
        present.append(location.stuffs[b].name)	
    for item in inventory:
        i += 1
        if invitem.lower() == inventory[i].name.lower() and inventory[i].useable == True:
            a = -1
            for item in location.stuffs:
                a += 1
                if target.lower() == location.stuffs[a].name.lower():
                    if location.stuffs[a].usefunction in content.usefunctions and location.stuffs[a].TargetOf.name.lower() == invitem.lower():
                        if location.stuffs[a].usefunction == "unlock":
                            content.unlock(location.stuffs[a])
                            location.stuffs.pop(a)
                            inventory.pop(i)
                            break
                        if location.stuffs[a].blocker.name in present:
                            print(location.stuffs[a].BlockedDescript)
                            break
                        if location.stuffs[a].usefunction == "mutate":
                            content.mutate(location.stuffs[a], location)
                            location.stuffs.pop(a)
                            inventory.pop(i)
                            break
                        if location.stuffs[a].usefunction == "move":
                            content.move(location.stuffs[a], location)
                            break
                elif a+1 == len(location.stuffs):
                    print("You can't use {} with that".format(inventory[i].name))
                    break
        if len(inventory) == 0:
            print("You have absolutely nothing.")
            break
        if i+1 == len(inventory) and invitem.lower() != inventory[i].name.lower():
            print("You don't have that item")
            break
        elif i+1 == len(inventory):
            print("That doesn't look useful")
            break

#EXAMINE ITEMS IN ROOM OR IN INVENTORY
def examine(target, location, inv):
    a = -1
    b = -1
    found = 0
    for item in location.stuffs:
        a += 1
        if target.lower() == location.stuffs[a].name.lower():
            print(location.stuffs[a].descript)
            found = 1
            break
    for item in inv:
        b += 1
        if target.lower() == inv[b].name.lower():
            print(inv[b].descript)
            found = 1
            break
    if a+1 == len(location.stuffs) and b+1 == len(inv) and found == 0:
        print("You don't see {} near you.".format(target))

#MOVE FUNCTION
def move(location, direction):
    prev_location = location
    if direction.lower() == "n" or direction.lower() == "north":
        current_location = location.nesw[0]
        return current_location, prev_location
    if direction.lower() == "e" or direction.lower() == "east":
        current_location = location.nesw[1]
        return current_location, prev_location
    if direction.lower() == "s" or direction.lower() == "south":
        current_location = location.nesw[2]
        return current_location, prev_location
    if direction.lower() == "w" or direction.lower() == "west":
        current_location = location.nesw[3]
        return current_location, prev_location
    else:
        print("I don't think that's a direction")
        return location, location




#MAIN LOOP
def run():
    running = True
    current_location = content.start_location
    print("you are in {} ".format(current_location.name) )
    while running:
        c= -1
        if current_location == content.wincondition:
            print(content.wintext)
            running = False
            break
        commandparts = input().split()
        if len(commandparts) > 0:
            command = commandparts[0]
            if command in commands:
                if command == "quit":
                    running = False
                    break

                if command == "help":
                    if len(commandparts) == 1:
                        print("You have" + " %s " % commands + "to use. \n for more help use \" help <command> \"")
                    if len(commandparts) == 2:
                        help(commandparts[1])
                    if len(commandparts) > 2:
                        print("You have" + " %s " % commands + "to use. \n for more help use \" help <command> \"")

                if command == "look":
                    look(current_location)

                if command == "take":
                    if len(commandparts) == 2:
                        take(current_location.stuffs, commandparts[1])
                    if len(commandparts) != 2:
                        help("take")
            
                if command == "combine":
                    if len(commandparts) == 4 and commandparts[2].lower() != "with":
                        help("combine")
                    if len(commandparts) != 4:
                        help("combine")
                    elif len(commandparts) == 4 and commandparts[2].lower() == "with":
                        combine(inventory, commandparts[1], commandparts[3])

                if command == "inventory":
                    for item in inventory:
                        c += 1
                        print(inventory[c].name)

                if command == "move":
                    if len(commandparts) != 2:
                        help("move")
                    if len(commandparts) == 2:
                        current_location, prev_location = move(current_location, commandparts[1])
                        if current_location.open == False:
                            print(current_location.blockdescript)
                            current_location = prev_location
                        elif current_location.open == True:
                            print("You are in {}".format(current_location.name))

                if command == "examine":
                    if len(commandparts) != 2:
                        help("examine")
                    if len(commandparts) == 2:
                        examine(commandparts[1], current_location, inventory)

                if command == "use":
                    if len(commandparts) != 4 or commandparts[2].lower() != "with":
                        help("use")
                    if len(commandparts) == 4 and commandparts[2].lower() == "with":
                        use(commandparts[1], commandparts[3], inventory, current_location)
    
            else:
                print("I don't know that")
        if len(commandparts) == 0:
            print(commands)

if __name__ == "__main__":
    sys.exit(run())
